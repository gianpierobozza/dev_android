package com.gbozza.aikiradio.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import com.gbozza.aikiradio.R;
import com.gbozza.aikiradio.utilities.MetadataUtils;

public class MetadataService extends Service {

	public static final String METADATA_ACTION = "MetadataServiceAction";

	private NotificationManager notificationManager;
	private Notification playerNotification;
	private SharedPreferences aikiPrefs;
	private Timer timer = new Timer();

	@Override
	public void onCreate() {
		super.onCreate();
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		aikiPrefs = this.getSharedPreferences(
			      "com.gbozza.aikiradio", Context.MODE_PRIVATE);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		TimerTask tt = new TimerTask() {
			
			@Override
			public void run() {
				URL url;
				String artist = "", title = "", cover_art_url = null;
				try {
					url = new URL(aikiPrefs
							.getString(getString(R.string.aikiRadioStreamUrlPref),
									"not_found"));
					IcyStreamMeta icy = new IcyStreamMeta(url);
					artist = icy.getArtist();
					title = icy.getTitle();
					cover_art_url = MetadataUtils.getAlbumCoverUrl(artist,
							title);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				Intent metadataIntent = new Intent(METADATA_ACTION);
				metadataIntent.putExtra("artist", artist);
				metadataIntent.putExtra("title", title);
				metadataIntent.putExtra("cover_art_url", cover_art_url);

				sendBroadcast(metadataIntent);
			}
		};
		timer.schedule(tt, 0, 10000);
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		timer.cancel();
		super.onDestroy();
	}
}
