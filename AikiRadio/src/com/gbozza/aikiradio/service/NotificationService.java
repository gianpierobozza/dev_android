package com.gbozza.aikiradio.service;

import com.gbozza.aikiradio.AikiActivity;
import com.gbozza.aikiradio.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.widget.TextView;

public class NotificationService extends Service {

	public final static String PLAYER_ACTION = "PlayerServiceAction";
	public final static String STOP_SERVICE = "";
	public final static int RQS_STOP_SERVICE = 1;
	public static final int PLAYER_NOTIFICATION_ID = 1;
	
	private NotificationManager notificationManager;
	private Notification playerNotification;

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@SuppressWarnings("deprecation")
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Send Notification
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Context context = getApplicationContext();
		
		int icon = android.R.drawable.ic_media_play;
		String ticker = getString(R.string.player_notification_ticker_playing);
		String title = getString(R.string.player_notification_title);
		String msg = getString(R.string.player_notification_msg);
		String s_artist = AikiActivity.song_artist.getText().toString();
		String s_title = AikiActivity.song_title.getText().toString();
		String full_msg = msg;
		
		if (s_artist != null && s_title != null) {
			full_msg += " " + s_artist + " - " + s_title;
		}
		playerNotification = new Notification(icon,
				ticker,
				System.currentTimeMillis());
		playerNotification.setLatestEventInfo(context,
				title,
				full_msg,
				PendingIntent.getActivity(this,
						0,
						new Intent(this, AikiActivity.class)
							.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP 
									| Intent.FLAG_ACTIVITY_SINGLE_TOP),
						PendingIntent.FLAG_CANCEL_CURRENT));
		playerNotification.flags |= Notification.FLAG_ONGOING_EVENT;

		notificationManager.notify(PLAYER_NOTIFICATION_ID, playerNotification);

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	public void onDestroy() {
		notificationManager.cancel(PLAYER_NOTIFICATION_ID);
		super.onDestroy();
	}
}
