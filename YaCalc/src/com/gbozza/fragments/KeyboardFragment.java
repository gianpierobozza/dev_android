package com.gbozza.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gbozza.yacalc.R;

public class KeyboardFragment extends Fragment {
	private Context calcContext;
	private TextView memoryStat;
	private View keyboardView;
	OnKeyPressed keyListener;
	Vibrator vibe;
	
	public interface OnKeyPressed {
		public void onKeyPressed(Button button);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.calcContext = (Context) activity;
		try {
			keyListener = (OnKeyPressed) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnKeyPressed");
		}
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		vibe = (Vibrator) calcContext.getSystemService(calcContext.VIBRATOR_SERVICE) ;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		keyboardView = inflater.inflate(R.layout.keyboard_fragment, container,
				false);
		memoryStat = (TextView) keyboardView.findViewById(R.id.memory_buffer);
		return keyboardView;
	}
	
	public void onClick(View view) {
		Button button = (Button)view;
		vibe.vibrate(20);
		keyListener.onKeyPressed(button);
	}

	public void memoryStatVisibility(int visibility) {
		memoryStat.setVisibility(visibility); 
	}
}
