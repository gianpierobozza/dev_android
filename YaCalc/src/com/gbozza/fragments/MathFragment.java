package com.gbozza.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Button;

import com.gbozza.yacalc.R;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;

public class MathFragment extends Fragment {
	private Calculable calc;
	private final String numericPattern = "^[0-9][0-9.]*$";
	private final String operatorsPattern = "^.*?[+|-|*|/].*$";
	private Double memoryBuffer;
	
	ArrayList<String> calcStack;
	
	public enum KeypadButton {
		MC("MC", KeypadButtonCategory.MEMORYBUFFER),
		MR("MR", KeypadButtonCategory.MEMORYBUFFER),
		MS("MS", KeypadButtonCategory.MEMORYBUFFER),
		M_ADD("M+", KeypadButtonCategory.MEMORYBUFFER),
		M_REMOVE("M-", KeypadButtonCategory.MEMORYBUFFER),
		BACK("<-", KeypadButtonCategory.CLEAR),
		CE("CE", KeypadButtonCategory.CLEAR),
		C("C", KeypadButtonCategory.CLEAR),
		ZERO("0", KeypadButtonCategory.NUMBER),
		ONE("1", KeypadButtonCategory.NUMBER),
		TWO("2", KeypadButtonCategory.NUMBER),
		THREE("3", KeypadButtonCategory.NUMBER),
		FOUR("4", KeypadButtonCategory.NUMBER),
		FIVE("5", KeypadButtonCategory.NUMBER),
		SIX("6", KeypadButtonCategory.NUMBER),
		SEVEN("7", KeypadButtonCategory.NUMBER),
		EIGHT("8", KeypadButtonCategory.NUMBER),
		NINE("9", KeypadButtonCategory.NUMBER),
		DEC(".", KeypadButtonCategory.DEC),
		PLUS("+", KeypadButtonCategory.OPERATOR),
		MINUS("-", KeypadButtonCategory.OPERATOR),
		MULT("*", KeypadButtonCategory.OPERATOR),
		DIV("/", KeypadButtonCategory.OPERATOR),
		RECIPROC("1/x", KeypadButtonCategory.UNARY),
		SIGN("±", KeypadButtonCategory.UNARY),
		SQRT("√", KeypadButtonCategory.UNARY),
		PERC("%", KeypadButtonCategory.UNARY),
		EQ("=", KeypadButtonCategory.RESULT);

		CharSequence mText; // Display Text
		KeypadButtonCategory mCategory;
		private static final Map<String, KeypadButton> lookup = new HashMap<String, KeypadButton>();
		static {
            for (KeypadButton d : KeypadButton.values())
                lookup.put(d.getText().toString(), d);
        }

		KeypadButton(CharSequence text, KeypadButtonCategory category) {
			mText = text;
			mCategory = category;
		}

		public CharSequence getText() {
			return mText;
		}
		
		public static String getCategory(String key) { 
			String tag = null;
			Log.d("CALC", "lookup for key: "+key);
			Object lookupRes = lookup.get(key).mCategory;
			Log.d("CALC", "lookup res: "+lookupRes);
			if (lookupRes != null) {
				tag = lookupRes.toString();
			}
			return tag;
		}
	}

	public enum KeypadButtonCategory {
		MEMORYBUFFER, NUMBER, NUMBERS, OPERATOR, CLEAR, RESULT, DEC, UNARY
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		// Create background worker threads and tasks.
		if (calcStack == null) {
			calcStack = new ArrayList<String>();
			calcStack.add("0");
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Initiate worker threads and tasks.
	}

	public String evaluate(String expression) {	
		String evaluatedString = "0";
		double evaluatedDouble = 0;

		try {
			calc = new ExpressionBuilder(expression).build();
			evaluatedDouble = calc.calculate();
		} catch (UnknownFunctionException e) {
			e.printStackTrace();
		} catch (UnparsableExpressionException e) {
			e.printStackTrace();
		}

		if (evaluatedDouble != 0) {
			evaluatedString = numberFormatString(evaluatedDouble);
		}

		return evaluatedString;	
	}
	
	public String evaluate(String lastCalcItem, String function) {
		String evaluatedString = null;
		double evaluatedDouble = 0;
		
		if (function == getString(R.string.key_reciproc)) {
			evaluatedDouble = 1 / Double.valueOf(lastCalcItem);
		} else if (function == getString(R.string.key_sign)) {
			evaluatedDouble = Double.valueOf(lastCalcItem);
			if (evaluatedDouble > 0) {
				evaluatedDouble = 0 - evaluatedDouble;
			} else if (evaluatedDouble < 0) {
				evaluatedDouble = Math.abs(evaluatedDouble);
			}
		} else if (function == getString(R.string.key_sqrt)) {
			evaluatedDouble = Math.sqrt(Double.valueOf(lastCalcItem));
		} 
		if (evaluatedDouble != 0) {
			evaluatedString = numberFormatString(evaluatedDouble);
		}
		
		return evaluatedString;
	}
	
	private String numberFormatString(double number) {
		String formattedString = String.valueOf(number);
		while (formattedString.endsWith("0")) {
			formattedString = (formattedString.substring(0,
					formattedString.length() - 1));
		}
		if (formattedString.endsWith(".")) {
			formattedString = (formattedString.substring(0,
					formattedString.length() - 1));
		}
		
		return formattedString;
	}
	
	private Double getUserInput() {
		double result = Double.NaN;
		String calcItem = null;
		
		for (int i = calcStack.size() - 1; i >= 0; i--) {
			calcItem = calcStack.get(i);
			if (calcItem.matches(numericPattern)) {
				try {
					result = Double.parseDouble(calcItem);
					break;
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
				}
			}
		}
		
		return result;
	}
	
	public String processKeypadInput(Button button) {
		String key = button.getText().toString();
		String lastStackItem = null;
		String newItem = "";
		String result = null;
		double userInputValue = Double.NaN;
		KeypadButton keypadButton = KeypadButton.valueOf(button.getTag().toString());
		int lastStackItemLen = 0;
		
		int lastStackPos = 0;
		if (calcStack.size() > 0) {
			lastStackPos = calcStack.size() - 1;
			lastStackItem = calcStack.get(lastStackPos);
			lastStackItemLen = lastStackItem.length();
		}
		
		Log.d("CALC", "item pressed: "+key);
		Log.d("CALC", "last stack item: "+lastStackItem);
		Log.d("CALC", "last stack item length: "+lastStackItemLen);
		
		switch (keypadButton) {
		case BACK:
			Log.d("CALC", "back key");
			Log.d("CALC", "lastStackItem: "+lastStackItem);
			int endIndex = lastStackItemLen - 1;
			if (endIndex < 1) {
				calcStack.remove(lastStackPos);
				newItem = "0";
			}
			else {
				calcStack.remove(lastStackPos);
				newItem = lastStackItem.subSequence(0, endIndex).toString();
			}
			calcStack.add(newItem);
			break;
		case SIGN:
			Log.d("CALC", "sign key");
			Log.d("CALC", "lastStackItem: "+lastStackItem);
			
			if (lastStackItemLen > 0 && !lastStackItem.equals("0")) {
				if (lastStackItem.charAt(0) == '-') {
					calcStack.remove(lastStackPos);
					newItem = lastStackItem.subSequence(1, lastStackItemLen).toString();
				}
				else {
					calcStack.remove(lastStackPos);
					newItem = "-" + lastStackItem;
				}
				calcStack.add(newItem);
			}
			break;
		case CE:
			Log.d("CALC", "CE key");
			calcStack.add("0");
			break;
		case C:
			Log.d("CALC", "C key");
			calcStack.clear();
			calcStack.add("0");
			break;
		case DEC:
			Log.d("CALC", "decimal sep key");
			if (lastStackItem.equals("0")) {
				calcStack.remove(lastStackPos);
				calcStack.add("0"+key);
			} else if (StringUtils.join(calcStack, "").contains(".")) {
				if (lastStackItemLen == 1 && KeypadButton.getCategory(lastStackItem) == KeypadButtonCategory.OPERATOR.toString()) {
					calcStack.add("0"+key);
				} else {
					int count = StringUtils.countMatches(StringUtils.join(calcStack, ""), ".");
					if (count == 1 && StringUtils.join(calcStack, "").matches(operatorsPattern)) {
						calcStack.add(key);
					} else {
						Log.d("CALC", "return null");
						return null;
					}
				}
			} else {
				if (KeypadButton.getCategory(lastStackItem) == KeypadButtonCategory.OPERATOR.toString()) {
					newItem = "0";
				}
				else {
					newItem = calcStack.get(lastStackPos);
				}
				newItem += key;
				calcStack.remove(lastStackPos);
				calcStack.add(newItem);
			}
			break;
		case EQ:
			if (calcStack.size() == 0) {
				break;
			}
			if (lastStackItem.contains(".") && lastStackItem.charAt(lastStackItemLen-1) == '.') {
				calcStack.remove(lastStackPos);
				calcStack.add(lastStackItem.subSequence(0, lastStackItemLen-1).toString());
			}
			result = evaluate(StringUtils.join(calcStack, ""));
			if (!result.equals("0")) {
				calcStack.clear();
				calcStack.add("0");
			}
			
			Log.d("CALC", "eval: "+result);
			break;
		case DIV:
		case PLUS:
		case MINUS:
		case MULT:
			if (calcStack.size() > 0) {
				if (StringUtils.join(calcStack, "").matches(operatorsPattern)) {
					Log.d("CALC","last stack item: "+lastStackItem);
					if (lastStackItem.matches(numericPattern)) {
						Log.d("CALC", "stack contains OPERATOR, return");
						return null;
					} 
					else {
						String lastStackItemCategory = KeypadButton.getCategory(lastStackItem);
						if (lastStackItemCategory == KeypadButtonCategory.OPERATOR.toString()
								&& KeypadButton.getCategory(key) == KeypadButtonCategory.OPERATOR.toString()) {
							calcStack.remove(lastStackPos);
							calcStack.add(key);
						}
					}
				}
				else {
					if (lastStackItem.contains(".") && lastStackItem.charAt(lastStackItemLen-1) == '.') {
						calcStack.remove(lastStackPos);
						calcStack.add(lastStackItem.subSequence(0, lastStackItemLen-1).toString());
					}
					calcStack.add(key);
				}
			}
			break;
		case MS:
			userInputValue = getUserInput();
			if (Double.isNaN(userInputValue)) {
				return null;
			}
			memoryBuffer = userInputValue;
			return key;
		case MC:
			memoryBuffer = Double.NaN;
			return key;
		default:
			Log.d("CALC", "default");
			Log.d("CALC", "lastStackItem: "+lastStackItem);
			if (lastStackItem.matches(numericPattern)) {
				Log.d("CALC", "is numeric");
				if (lastStackItem.equals("0")) {
					Log.d("CALC", "and is ZERO");
					newItem = key;
				} else {
					Log.d("CALC", "and is "+calcStack.get(lastStackPos));
					newItem = calcStack.get(lastStackPos) + key;
				}
				calcStack.remove(lastStackPos);
			}
			else {
				newItem = key;
			}
			calcStack.add(newItem);
			break;
		}
		
		Log.d("CALC", "stack: "+StringUtils.join(calcStack, ""));
		if (result == null && calcStack.size() != 0) {
			result = "#"+StringUtils.join(calcStack, "");
		} else {
			result ="@"+result;
		}
		return result;
	}

}
