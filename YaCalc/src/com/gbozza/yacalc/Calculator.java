package com.gbozza.yacalc;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.ClipboardManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.gbozza.fragments.DisplayFragment;
import com.gbozza.fragments.KeyboardFragment;
import com.gbozza.fragments.KeyboardFragment.OnKeyPressed;
import com.gbozza.fragments.MathFragment;

@SuppressWarnings("deprecation")
public class Calculator extends FragmentActivity implements OnKeyPressed {
	private static String MATH_FRAGMENT = "MATH_FRAGMENT";
	private static String INFINITY = "Infinity";
	//private String alphanumericPattern = "^[a-zA-Z0-9]*$";
	
	DisplayFragment displayFragment;
	KeyboardFragment keyboardFragment;
	MathFragment mathFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator);

		FragmentManager fm = getSupportFragmentManager();

		displayFragment = (DisplayFragment) fm
				.findFragmentById(R.id.display_container);
		keyboardFragment = (KeyboardFragment) fm
				.findFragmentById(R.id.keyboard_container);
		mathFragment = (MathFragment) fm
				.findFragmentByTag(MATH_FRAGMENT);
		if (displayFragment == null && keyboardFragment == null
				&& mathFragment == null) {
			FragmentTransaction ft = fm.beginTransaction();
			displayFragment = new DisplayFragment();
			keyboardFragment =  new KeyboardFragment();
			mathFragment =  new MathFragment();
			ft.add(R.id.display_container, displayFragment);
			ft.add(R.id.keyboard_container, keyboardFragment);
			ft.add(mathFragment, MATH_FRAGMENT);
			
			ft.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_calculator, menu);
		return true;
	}
	
	@SuppressWarnings("static-access" )
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.menu_clear_clipboard: // Clear Clip board
	        	ClipboardManager clipManager = (ClipboardManager) 
				getSystemService(getBaseContext().CLIPBOARD_SERVICE);
	        	clipManager.setText(null);
	            return true;
	        case R.id.menu_settings: // Settings
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	@Override
	public void onKeyPressed(Button button) {
		String mathResult = mathFragment.processKeypadInput(button);
		Boolean append = false;
		if (mathResult != null) {
			if (mathResult.charAt(0) == 'M') {
				if (mathResult.equals("MS")) {
					keyboardFragment.memoryStatVisibility(View.VISIBLE);
				} else if (mathResult.equals("MC")) {
					keyboardFragment.memoryStatVisibility(View.GONE);
				}
			} else {			
				if (mathResult.charAt(0) == '#') {
					append = true;
				}
				mathResult = mathResult.subSequence(1, mathResult.length()).toString();
				displayFragment.updateDisplay(mathResult, append);
				if (mathResult.equals(INFINITY)) {
					displayFragment.updateDisplay("0", append);
				}
			}
		}
	}
	
	public void mymet() {
		
	}
	
	public void onClick(View view) {
		keyboardFragment.onClick(view);
	}
	
}
