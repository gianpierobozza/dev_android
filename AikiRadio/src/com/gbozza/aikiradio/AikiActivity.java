package com.gbozza.aikiradio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.PowerManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.gbozza.aikiradio.service.MetadataService;
import com.gbozza.aikiradio.service.NotificationService;
import com.gbozza.aikiradio.utilities.AppStatus;

public class AikiActivity extends Activity implements OnPreparedListener {
	
	// Public section
	public static TextView song_artist;
	public static TextView song_title;

	// Protected section
	MediaPlayer aikiradioMediaPlayer = null;

	// Private section
	private AQuery aq;
	private String aikiRadioStreamUrl = null;
	private Boolean playerPrepared = false, playerPreparing = false;
	private Intent metadataIntent, notificationIntent;
	private long lastBackPressTime = 0;
	private NotificationManager notificationManager;
	private Notification playerNotification;
	private Toast toast;
	private SharedPreferences aikiPrefs;
	private WifiLock wifiLock = null;

	private Button playbackButton;
	private ImageView cover_art;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aiki);

		AikiRater.app_launched(this);
		initActivity();
	}

	private void initActivity() {
		aq = new AQuery(this);
		song_artist = (TextView) findViewById(R.id.song_artist);
		song_title = (TextView) findViewById(R.id.song_title);
		cover_art = (ImageView) findViewById(R.id.cover_art);
		playbackButton = ((Button) findViewById(R.id.playbackButton));
		playbackButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.pause_red));
		playbackButton.setEnabled(false);
		
		song_artist.setText(getText(R.string.no_artist_label));
		song_title.setText(getText(R.string.no_title_label));
		cover_art.setImageResource(R.drawable.aiki_no_cover);
		
		metadataIntent = new Intent(this, MetadataService.class);
		metadataIntent.setAction(MetadataService.METADATA_ACTION);
		
		notificationIntent = new Intent(this, NotificationService.class);
		notificationIntent.setAction(NotificationService.PLAYER_ACTION);
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        
        aikiPrefs = this.getSharedPreferences(
			      "com.gbozza.aikiradio", Context.MODE_PRIVATE);

		Thread networkThread = new Thread() {
			@Override
			public void run() {
				try {
					BufferedReader in = null;
					InputStreamReader isr = null;
					StringBuffer result = new StringBuffer();
					try {
						URL jsonUrl = new URL(getString(R.string.aikiRadioStreamTxt));
						isr = new InputStreamReader(jsonUrl.openStream());
						in = new BufferedReader(isr);
						String inputLine;
						while ((inputLine = in.readLine()) != null) {
							result.append(inputLine);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
					in.close();
					isr.close();
					aikiPrefs
							.edit()
							.putString(
									getString(R.string.aikiRadioStreamUrlPref),
									result.toString()).commit(); 
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		networkThread.start();
		aikiPlayerPrepare();
	}
	
	private void startServices() {
		registerReceiver(broadcastReceiver, new IntentFilter(MetadataService.METADATA_ACTION));
		registerReceiver(broadcastReceiver, new IntentFilter(NotificationService.PLAYER_ACTION));
		startService(metadataIntent);
		startService(notificationIntent);
		if (!aikiradioMediaPlayer.isPlaying()) {
			playbackControl(getWindow().getDecorView().findViewById(android.R.id.content));
		}
	}

	private void aikiPlayerPrepare() {
		playerPreparing = true;
		if (AppStatus.getInstance(this).isOnline(this)) {
			aikiradioMediaPlayer = new MediaPlayer();
			aikiradioMediaPlayer.setOnPreparedListener(this);
			aikiradioMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			try {
				aikiradioMediaPlayer.setDataSource(aikiPrefs
						.getString(getString(R.string.aikiRadioStreamUrlPref),
								"not_found"));
				aikiradioMediaPlayer.setWakeMode(getApplicationContext(),
						PowerManager.PARTIAL_WAKE_LOCK);
				aikiradioMediaPlayer.prepareAsync();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			playerPreparing = false;
			cover_art.setImageResource(R.drawable.aiki_no_cover);
			toast = Toast.makeText(this,
					getText(R.string.toast_check_connection), 50000);
			toast.show();
		}
	}

	@Override
	public void onPrepared(MediaPlayer aikiradioMediaPlayer) {
		playerPreparing = false;
		wifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
				.createWifiLock(WifiManager.WIFI_MODE_FULL, "aikiradioWifiLock");
		wifiLock.acquire();
		
		aikiradioMediaPlayer.start();
		playbackButton.setEnabled(true);
		playbackButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.play_green));
		playerPrepared = true;
	}

	public void playbackControl(View view) {
		if (aikiradioMediaPlayer.isPlaying()) { // Pausing
			playbackButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.pause_red));
			if (wifiLock != null && wifiLock.isHeld()) {
				wifiLock.release();
			}
			playerNotification(android.R.drawable.ic_media_pause,
					getString(R.string.player_notification_ticker_paused),
					getString(R.string.player_notification_title),
					getString(R.string.player_notification_msg),
					song_artist.getText().toString(),
					song_title.getText().toString());
			aikiradioMediaPlayer.pause();
		} else { // Re-Starting
			playbackButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.play_green));
			if (wifiLock != null && !wifiLock.isHeld()) {
				wifiLock.acquire();
			}
			playerNotification(android.R.drawable.ic_media_play,
					getString(R.string.player_notification_ticker_playing),
					getString(R.string.player_notification_title),
					getString(R.string.player_notification_msg),
					song_artist.getText().toString(),
					song_title.getText().toString());
			if (playerPrepared) {
				aikiradioMediaPlayer.start();
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	private void playerNotification(int icon, String ticker, String title, String msg, String s_artist, String s_title) {
		String full_msg = msg;
		if (s_artist != null && s_title != null) {
			full_msg += " " + s_artist + " - " + s_title;
		}
		playerNotification = new Notification(icon, ticker,
				System.currentTimeMillis());
		playerNotification.setLatestEventInfo(getApplicationContext(), title,
				full_msg, PendingIntent.getActivity(this, 0, new Intent(this,
						AikiActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP),
						PendingIntent.FLAG_CANCEL_CURRENT));
		playerNotification.flags |= Notification.FLAG_ONGOING_EVENT;

		notificationManager.notify(NotificationService.PLAYER_NOTIFICATION_ID,
				playerNotification);
	}
	
	@SuppressWarnings("deprecation")
	private void playerNotificationUpdate(String artist, String title, Boolean conn) {
		Intent intent = new Intent(this, AikiActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(
				getApplicationContext(), 0, intent,
				Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		String full_msg = getString(R.string.player_notification_msg);
		if (artist != null && title != null && conn) {
			// Connection available and track info provided
			full_msg += " " + artist + " - " + title;
		} else if (!conn) {
			// Connection not available
			full_msg = getString(R.string.player_notification_msg_stopped);
			playerNotification = new Notification(android.R.drawable.ic_media_pause,
					getString(R.string.player_notification_msg_stopped),
					System.currentTimeMillis());
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			playerNotification.flags |= Notification.FLAG_ONGOING_EVENT;
		}
		
		playerNotification.setLatestEventInfo(getApplicationContext(),
				getString(R.string.player_notification_title),
				full_msg,
				pendingIntent);

		notificationManager.notify(NotificationService.PLAYER_NOTIFICATION_ID,
				playerNotification);
	}

	@Override
	public void onBackPressed() {
		if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
			toast = Toast.makeText(this, getText(R.string.toast_exit_text),
					4000);
			toast.show();
			this.lastBackPressTime = System.currentTimeMillis();
		} else {
			if (toast != null) {
				toast.cancel();
			}
			// Stop the player and exit
			if (playerPrepared) {
				if (aikiradioMediaPlayer.isPlaying()) {
					aikiradioMediaPlayer.stop();
				}
				aikiradioMediaPlayer.release();
				aikiradioMediaPlayer = null;
			}
			if (wifiLock != null && wifiLock.isHeld()) {
				wifiLock.release();
			}
			stopService(metadataIntent);
			stopService(notificationIntent);
			super.onBackPressed();
		}
	}
	
	public void onClickSpinner(View v) {
		Uri uri = Uri.parse(getString(R.string.aikiRadioSiteUrl));
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_aiki, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.menu_info: // Info
	        	AlertDialog.Builder info_alert = new AlertDialog.Builder(this);
	        	info_alert.setTitle(getText(R.string.info_menu_title));
	        	info_alert.setMessage(getText(R.string.info_menu_msg));
	        	info_alert.setNegativeButton(getText(R.string.info_menu_close_button), new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int id) {
	                	dialog.dismiss();
	                }
	            });
	        	AlertDialog alert = info_alert.create();
	        	alert.show();
	            return true;
	        case R.id.menu_exit: // Exit
				if (playerPrepared) {
					if (aikiradioMediaPlayer.isPlaying()) {
						aikiradioMediaPlayer.stop();
					}
					aikiradioMediaPlayer.release();
					aikiradioMediaPlayer = null;
				}
				if (wifiLock != null && wifiLock.isHeld()) {
					wifiLock.release();
				}
				stopService(metadataIntent);
				stopService(notificationIntent);
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			ConnectivityManager conn = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = conn.getActiveNetworkInfo();

			if (networkInfo != null	&& (networkInfo.getType() == ConnectivityManager.TYPE_WIFI || networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)) {
				if (playerPrepared == false && playerPreparing == false) {
					initActivity();
					startServices();
					playerPreparing = true;
				}
			} else {
				if (playerPrepared) {
					playerNotificationUpdate(null, null, false);
					song_artist.setText("");
					song_title.setText("");
					cover_art.setImageResource(R.drawable.aiki_no_cover);
					playbackButton.setEnabled(false);
					playbackButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.pause_red));
					if (playerPrepared && aikiradioMediaPlayer.isPlaying()) {
						aikiradioMediaPlayer.stop();
					}
					if (wifiLock != null && wifiLock.isHeld()) {
						wifiLock.release();
					}
					stopService(metadataIntent);
					playerPrepared = false;
				}
			}

			int rqs = intent.getIntExtra("RQS", 0);
			if (rqs == NotificationService.RQS_STOP_SERVICE) {
				startService(notificationIntent);
			}
			else {
				updateUI(intent);
			}
		}
	};
	
	private final PhoneStateListener phoneStateListener = new PhoneStateListener() {
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state) {
			case TelephonyManager.CALL_STATE_RINGING:
				playbackControl(getWindow().getDecorView().findViewById(android.R.id.content));
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				playbackControl(getWindow().getDecorView().findViewById(android.R.id.content));
				break;
			}
			super.onCallStateChanged(state, incomingNumber);
		}
	};

	private void updateUI(Intent intent) {
		String artist = intent.getStringExtra("artist");
		String title = intent.getStringExtra("title");
		String cover_art_url = intent.getStringExtra("cover_art_url");

		if (artist != null && title != null) {
			if (song_artist.getText() != artist	&& song_title.getText() != title) {
				song_artist.setText(artist);
				song_title.setText(title);
				if (cover_art_url != null) {
					aq.id(cover_art).image(cover_art_url);
				} else {
					cover_art.setImageResource(R.drawable.aiki_no_cover);
				}
				playerNotificationUpdate(artist, title,
						AppStatus.getInstance(this).isOnline(this));
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		if (AppStatus.getInstance(this).isOnline(this)) {
			startServices();
		}
	}
	
	public void onDestroy() {
		super.onDestroy();
		if (AppStatus.getInstance(this).isOnline(this)) {
			stopService(metadataIntent);
		}
		unregisterReceiver(broadcastReceiver);
	}
}