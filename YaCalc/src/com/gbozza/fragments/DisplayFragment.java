package com.gbozza.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gbozza.yacalc.R;

@SuppressWarnings("deprecation")
public class DisplayFragment extends Fragment {
	private static String CALC_ITEMS = "CALC_ITEMS";
	private final static int DIALOG_ACTION_COPY = 0;
	private final static int DIALOG_ACTION_CUT = 1;
	private final static int DIALOG_ACTION_DELETE = 2;
	private final static int DIALOG_ACTION_PASTE = 3;
	private Context calcContext;
	private View displayView;

	ListView displayListView;
	ArrayList<String> calcItems;
	ArrayAdapter<String> adapter;
	AlertDialog.Builder contextDialog;
	ClipboardManager clipManager;
	List<String> menuListItems;
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.calcContext = (Context) activity;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		displayView = inflater.inflate(R.layout.display_fragment, container,
				false);

		if (savedInstanceState != null
				&& savedInstanceState.containsKey(CALC_ITEMS)) {
			calcItems = savedInstanceState.getStringArrayList(CALC_ITEMS);
		}
		
		displayListView = (ListView) displayView.findViewById(R.id.displayListView);
		if (calcItems == null) {
			calcItems = new ArrayList<String>();
		}
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.display_listview_element,
				calcItems);
		displayListView.setAdapter(adapter);
		
		displayListView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View displayView,
					int position, long id) {
				TextView item = (TextView) displayView;
                
                createContextDialog(item, position);
                return true;
			}		
		});

		return displayView;
	}
	
	@SuppressWarnings("static-access")
	protected void createContextDialog(final TextView item, final int position) {
		clipManager = (ClipboardManager) calcContext
				.getSystemService(calcContext.CLIPBOARD_SERVICE);
		contextDialog = new AlertDialog.Builder(calcContext);
		contextDialog.setTitle(getString(R.string.dialog_action_title));
		menuListItems = new ArrayList<String>();
		menuListItems.add(getString(R.string.dialog_action_copy));
		menuListItems.add(getString(R.string.dialog_action_cut));
		menuListItems.add(getString(R.string.dialog_action_delete));
		if (clipManager.hasText()) {
			menuListItems.add(getString(R.string.dialog_action_paste));
		}
		final CharSequence[] contextMenu = menuListItems
				.toArray(new CharSequence[menuListItems.size()]);

		contextDialog.setItems(contextMenu, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int menuIndex) {
				switch (menuIndex) {
					case DIALOG_ACTION_COPY:
						copyItem(item.getText().toString());
						break;
					case DIALOG_ACTION_CUT:
						copyItem(item.getText().toString());
						deleteItem(position);
						break;
					case DIALOG_ACTION_DELETE:
						deleteItem(position);
						break;
					case DIALOG_ACTION_PASTE:
						pasteItem();
						break;
					default:
						break;
				}
			}
			
			private void copyItem(String item) {
				clipManager.setText(item);
			}
			
			private void deleteItem(int position) {
				calcItems.remove(position);
				adapter.notifyDataSetChanged();
			}
			
			private void pasteItem() {
				String item = clipManager.getText().toString();
				String lastItem = getLastCalcItem();
				if (!lastItem.equals("0")) {
					item = lastItem + item;
					calcItems.remove(0);
				}
				calcItems.add(0, item);
				adapter.notifyDataSetChanged();
			}
        });

		contextDialog.show();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putStringArrayList(CALC_ITEMS, calcItems);
		super.onSaveInstanceState(outState);
	}
	
	public void updateDisplay(String calcItem, Boolean append) {
		displayListView.smoothScrollToPosition(0);
		if (append && calcItems.size() != 0) {
			calcItems.remove(0);
		}
		calcItems.add(0, calcItem);
			
		adapter.notifyDataSetChanged();
	}
	
	public String getLastCalcItem() {
		String lastCalcItem = "0";
		if (calcItems.size() != 0) {
			lastCalcItem = calcItems.get(0);
		}
		return lastCalcItem;
	}
	
}
