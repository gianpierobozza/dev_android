package com.gbozza.aikiradio.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class MetadataUtils {

	public static String getAlbumCoverUrl(String artist, String song_name) {
		URL u = null;
		URLConnection uc = null;
		String album_cover = null;
		// API Key: 5c8024d276b7c5a373afa4ed8e68cade
		// Secret: is fe5237bec0bbec7719824dae4c5aecf0
		// Account last.fm gianpierobozza
		if (artist == null || song_name == null) {
			artist = song_name = "";
		}
		String album_cover_url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=5c8024d276b7c5a373afa4ed8e68cade&format=json&artist="
				+ artist.replace(" ", "%20")
				+ "&track="
				+ song_name.replace(" ", "%20");
		try {
			u = new URL(album_cover_url);
			uc = u.openConnection();
			u.openConnection().setConnectTimeout(15000);
			uc.connect();

			InputStream in = uc.getInputStream();
			String instream = convertStreamToString(in);

			JSONObject jobject = new JSONObject(instream);
			album_cover = jobject.getJSONObject("track").getJSONObject("album")
					.getJSONArray("image").getJSONObject(1).getString("#text");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return album_cover;
	}

	protected static String convertStreamToString(InputStream is)
			throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}
}
